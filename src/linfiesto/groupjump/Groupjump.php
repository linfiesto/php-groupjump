<?php 

namespace linfiesto\groupjump;

class Groupjump {

	public $curl;

	public function __construct(){
		$this->curl = new \anlutro\cURL\cURL;
	}

	private function newJumpsiteCurl($method, $url, $data = [], $headers = []){

		$groupjumpcurl = $this->curl->newRequest($method, $url, $data)
		->setOption(CURLOPT_SSL_VERIFYPEER, false);
		foreach($headers as $key => $value){
			$groupjumpcurl->setHeader($key, $value);
		}
		
		return $groupjumpcurl->send();
	}

	private function newFileCurl($method, $url, $data = [], $headers = []){

		$groupjumpcurl = $this->curl->newRawRequest($method, $url, $data)
		->setOption(CURLOPT_SSL_VERIFYPEER, false)
		->setOption(CURLOPT_HEADER, true)
        ->setOption(CURLOPT_POSTFIELDS, $data);
		foreach($headers as $key => $value){
			$groupjumpcurl->setHeader($key, $value);
		}
		return $groupjumpcurl->send();
	}

	public function newLogin($method, $url, $data){
		
		$checkCurl = $this->newJumpsiteCurl($method, $url, $data);
		$code = 404;
		if ($checkCurl->code == 200){
			$postdata = json_decode($checkCurl);
			$session = new Session();
            $session->start();
            $session->set('user', $postdata->data);
            $session->set('loggedin', true);
            $code = 200;
        }
        $postdata->code = $code;
		return $postdata;
	}

	public function newHomefeeds($method, $url){

		$checkCurl = $this->newJumpsiteCurl($method, $url);
		if ($checkCurl->code == 200){
				$postdata = json_decode($checkCurl)->data->data;
           	}
        return $postdata;
	}

	public function newTrending($method, $url){
		
		$checkCurl = $this->newJumpsiteCurl($method, $url);

		if ($checkCurl->code == 200){
				$postdata = json_decode($checkCurl)->data->data;
           	}
        return $postdata;
	}

	public function newTopStories($method, $url){

		$checkCurl = $this->newJumpsiteCurl($method, $url);

		if ($checkCurl->code == 200){
				$postdata = json_decode($checkCurl)->data->data;
           	}
        return $postdata;
	}

	public function newRegister($method, $url, $data){

		$checkCurl = $this->newJumpsiteCurl($method, $url, $data);

		if ($checkCurl->code == 200){
				$postdata = json_decode($checkCurl);
            	return $postdata;
           	}
        else {
        	echo "error registering";
        }
	}

	public function newMorePost($method, $url, $headers){

		$checkCurl = $this->newJumpsiteCurl($method, $url, [], $headers);

		if ($checkCurl->code == 200){
				$postdata = json_decode($checkCurl);
				$postdata = new Response(json_encode($postdata->data->posts->data));
           	}
        return $postdata;
	}

	public function newFileUpload($method, $url, $data, $headers){

		$checkCurl = $this->newFileCurl($method, $url, $data, $headers);
		$uploadedavatar = json_decode($checkCurl);

		return $uploadedavatar;
	}

	public function newAvatar($method, $url, $data, $headers){
		$checkCurl = $this->newJumpsiteCurl($method, $url, $data, $headers);
		$postdata = json_decode($checkCurl);

		return $postdata;
	}

	public function newProfile($method, $url, $data, $headers){

		$postdata = $this->newJumpsiteCurl($method, $url, $data, $headers);

		return $postdata;
	}

	public function Like($method, $url, $headers){
		$postdata = $this->newJumpsiteCurl($method, $url, [], $headers);

		return $postdata;
	}

	public function deleteLike($method, $url, $headers){
		$postdata = $this->newJumpsiteCurl($method, $url, [], $headers);

		return $postdata;
	}	

	public function newComment($method, $url, $data, $headers){
		$postdata = $this->newJumpsiteCurl($method, $url, $data, $headers);

		return $postdata;
	}	

	public function deleteComment($method, $url, $headers){
		$postdata = $this->newJumpsiteCurl($method, $url, [], $headers);
		
		return $postdata;
	}	

	public function newPostBlog($method, $url, $data, $headers){
		$postdata = $this->newJumpsiteCurl($method, $url, $data, $headers);
		if ($postdata->code == 200){}
		return $postdata;
	}

	public function deletePost($method, $url, $headers){
		$postdata = $this->newJumpsiteCurl($method, $url, [], $headers);
		if ($postdata->code == 200){}
		return $postdata;
	}

	public function newSinglePost($method, $url, $headers){
		$checkCurl = $this->newJumpsiteCurl($method, $url, [], $headers);
		if ($checkCurl->code == 200){
			$postdata = json_decode($checkCurl)->post;
		}
		return $postdata;
	}

	public function newJumpzoneFeeds($method, $url, $headers){
		$checkCurl = $this->newJumpsiteCurl($method, $url, [], $headers);
		if ($checkCurl->code == 200){
			$postdata = json_decode($checkCurl)->data->posts;
		}
		return $postdata;
	}

}
